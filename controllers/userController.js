var User = require('../models/user');

exports.create = function(req, res){
    res.render('user/create');
};

// Display list of all Authors.
exports.edit = function(req, res) {
    User.findById(req.params.id)
        .exec(function (err, user) {
            res.render('user/edit', { title: 'Users Index ', user: user });
        });
};

// Display list of all Authors.
exports.update = function(req, res) {
    User.findOneAndUpdate({_id: req.params.id}, {$set:req.body}, {returnOriginal: false}, (err, doc) => {
        res.redirect(doc.editRoute);
    });
};

exports.store = function(req,res){
    var user = new User(req.body);

    user.save();
    res.redirect(user.showRoute);
}
// Display detail page for a specific Author.
exports.delete = function(req, res) {
    User.deleteOne({'_id':req.params.id})
        .exec(function (err) {
            res.redirect('/users');
        });
};

// Display list of all Authors.

exports.index = function(req, res) {
    User.find().exec(function (err,users) {
        res.render('user/index',{ title: 'Users Index ', users:  users})
    })
};

// Display list of all Authors.
exports.show = function(req, res) {
    User.findById(req.params.id)
        .exec(function (err, user) {
            res.render('user/show', { title: 'Users Index ', user: user });
        });
};