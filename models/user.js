var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var UserSchema = new Schema(
    {
        _id: {type:ObjectIdSchema, default: new ObjectId(), required: true},
        first_name: {type: String, required: true, max: 100},
        last_name: {type: String, required: true, max: 100},
        age: {type: Number, required: true, max: 100},
        country: {type: String, required: true, max: 100},
    },{
        collection: 'users'
    }
);

// Virtual for author's full name
UserSchema
    .virtual('name')
    .get(function () {
        return this.first_name + ', ' + this.last_name;
    });

// Virtual for show link
UserSchema
    .virtual('showRoute')
    .get(function () {
        return '/users/' + this._id;
    });

// Virtual for edit link
UserSchema
    .virtual('editRoute')
    .get(function () {
        return '/users/' + this._id + '/edit';
    });

UserSchema
    .virtual('deleteRoute')
    .get(function () {
        return '/users/' + this._id + '/delete';
    });

//Export model
module.exports = mongoose.model('User', UserSchema);
