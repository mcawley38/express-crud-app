var express = require('express');
var router = express.Router();
var user_controller = require('../controllers/userController');

/* GET users listing. */
router.get('/', user_controller.index);

/* UPDATE individual user data. */
router.get('/create', user_controller.create);

/* UPDATE individual user data. */
router.post('/create', user_controller.store);

/* GET individual user data. */
router.get('/:id', user_controller.show);

/* GET individual user data for editing. */
router.get('/:id/edit', user_controller.edit);

/* UPDATE individual user data. */
router.post('/:id/edit', user_controller.update);

/* DELTE individual user data. */
router.post('/:id/delete', user_controller.delete);

module.exports = router;
